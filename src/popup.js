let currentActiveMenu = 0;

const navLeft = document.querySelector(".navigation .left");
const navRight = document.querySelector(".navigation .right");

const setActiveMenu = (menu, direction) => {
    if (0 <= menu && menu <= document.querySelectorAll('.menu').length - 1) {
        document.querySelectorAll('.menu').forEach((menu) => {
            menu.classList.remove('isActive');
        });
        document.querySelectorAll('.menu')[menu].classList.add('isActive');
        if (direction === 'backward') {
            currentActiveMenu--
        } else if (direction === 'forward') {
            currentActiveMenu++
        }
    }
};

navLeft.addEventListener('click', () => {
    setActiveMenu(currentActiveMenu - 1, 'backward');
});

navRight.addEventListener('click', () => {
    setActiveMenu(currentActiveMenu + 1, 'forward');
});

let menus = [];

const crous = () => {
    for (let currentMenu = 0; currentMenu < menus.length; currentMenu++) {

        let div = document.createElement('div');
        div.classList.add('menu');

        document.querySelector('.content').appendChild(div);
        let menuOfTheDay = menus[currentMenu]['menuContent'];

        for (let entry in menuOfTheDay) {
            let div = document.createElement('div');
            div.classList.add('entry');
            let emoji;
            switch (entry) {
                case 'entree':
                    emoji = "🥗";
                    break;
                case 'plat':
                    emoji = "🥘";
                    break;
                case 'dessert':
                    emoji = "🍰";
                    break;
                default:
                    emoji = "🙄"
            }
            div.innerHTML += `<div class="badge">${emoji}</div>`;

            let currentEntry = 0;

            let date = document.createElement('div');
            date.classList.add('date');
            date.innerHTML = menus[currentMenu]['menuDate'];
            document.querySelector(`.content .menu:nth-child(${currentMenu + 1})`).appendChild(date);

            menus[currentMenu]['menuContent'][entry].forEach((item) => {
                div.innerHTML += `<div>${item}</div>`;
            });
            document.querySelector(`.content .menu:nth-child(${currentMenu + 1})`).appendChild(div);
        }
    }
    setActiveMenu(currentActiveMenu);
};

const fetchCrous = () => {
    const URL = 'http://www.crous-lyon.fr/restaurant/restaurant-de-bron-2/';
    crousHeaders = new Headers({
        "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
        "Accept-Language": "en-US,en;q=0.7,fr;q=0.3",
        "Accept-Encoding": "gzip, deflate, br",
        "Cookie": " cb-enabled=enabled",
        "DNT": "1",
        "Connection": "keep-alive",
        "Upgrade-Insecure-Requests": " 1",
        "Pragma": " no-cache",
        "Cache-Control": "no-cache"
    });
    return fetch(new Request(URL), {
        cache: "no-cache",
        headers: crousHeaders
    })
        .then(response => {
            if (response.ok) {
                return response.text();
            }
            throw new Error("Internet disconnected");
        })
        .then(response => {
            let parser = new DOMParser();
            let crous = parser.parseFromString(response, "text/html");

            let element = document.querySelector('.overlay');
            element.parentNode.removeChild(element);

            crous.querySelectorAll('#menu-repas .slides > li').forEach(table => {
                let menuDate = table.querySelector('h3').innerHTML;
                let menu = table.querySelector('.content.clearfix > div:nth-child(2) .content-repas > div');
                let menuContent = menu.querySelectorAll('ul');
                const parseMenuContent = (content) => {
                    let array = [];
                    content.querySelectorAll('li').forEach((entry) => {
                        array.push(entry.innerText);
                    });
                    return array;
                };
                menus.push({
                    menuDate,
                    menuContent: {
                        entree: parseMenuContent(menuContent[0]),
                        plat: parseMenuContent(menuContent[1]),
                        dessert: parseMenuContent(menuContent[2])
                    }
                });
            });
        })
        .then(crous)
        .catch(error => {
            console.error(error);
        });
};

fetchCrous();
