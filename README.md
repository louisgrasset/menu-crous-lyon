# menu-crous-lyon

Extension permettant de récupérer le menu du jour du restaurant Crous de Bron.


Available here [Firefox Add-ons](https://addons.mozilla.org/firefox/addon/menu-crous-lyon/) | [Chrome Web Store](https://chrome.google.com/webstore/detail/ooplbihggkhoomnekbgmcikllgfnknbi).

![menu-crous-lyon screenshot](menu-crous-lyon-screenshot.png)

## Build
Simply clone the repo, ``cd`` in then run ``sh ./dist.sh``.
*Dependencies : you'll need the `zip` command installed in your system.*