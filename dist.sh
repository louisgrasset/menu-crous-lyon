#!/usr/bin/env sh
rm -R build 2>/dev/null
mkdir "build"
cd ./src/
npm install
cd ../
cp -R ./src/*.* ./src/icons/ ./src/node_modules/ ./build/
cd ./build/
zip -r ../build/build.zip ./
cd ../
rm -R dist 2>/dev/null
mkdir "dist"
cp ./build/build.zip ./dist/menu-crous-lyon-chrome-X.XX.zip
mv ./build/build.zip ./dist/menu-crous-lyon-firefox-X.XX.xpi
